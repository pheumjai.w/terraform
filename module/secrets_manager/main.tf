resource "aws_secretsmanager_secret" "secretmanager" {
  name = var.name
  description = var.description
}