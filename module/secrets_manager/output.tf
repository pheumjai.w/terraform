output "arn" {
  value = aws_secretsmanager_secret.secretmanager.arn
}

output "name" {
  value = aws_secretsmanager_secret.secretmanager.name
}