variable "environment" {
  description = "infrastructure environment"
}

variable "region" {
  description = "infrastructure region"
}

variable "vpc_cidr" {
  description = "vpc cidr block"
}

variable "availability_zone" {
  type        = list(string)
  description = "availability zones"
}

variable "public_subnet_cidr" {
  type        = list(string)
  description = "public subnet cidr block"
}

variable "private_subnet_cidr" {
  type        = list(string)
  description = "private subnet cidr block"
}

variable "public_nacl_inbound_rule" {
  description = "network acl rule"
  type        = list(map(string))
  default = [{
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  }]
}

variable "public_nacl_outbound_rule" {
  description = "network acl rule"
  type        = list(map(string))
  default = [{
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  }]
}

variable "private_nacl_inbound_rule" {
  description = "network acl rule"
  type        = list(map(string))
  default = [{
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  }]
}

variable "private_nacl_outbound_rule" {
  description = "network acl rule"
  type        = list(map(string))
  default = [{
    rule_number = 100
    rule_action = "allow"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_block  = "0.0.0.0/0"
  }]
}