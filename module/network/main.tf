# VPC Section
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name        = "${var.environment}_vpc"
    Environment = "${var.environment}"
  }
}

# Public Subnet
resource "aws_subnet" "public_subnet" {
  vpc_id = aws_vpc.vpc.id

  count                   = length(var.public_subnet_cidr)
  cidr_block              = element(var.public_subnet_cidr, count.index)
  availability_zone       = element(var.availability_zone, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name        = "${var.environment}-public-subnet-${element(var.availability_zone, count.index)}"
    Environment = "${var.environment}"
  }
}

# Private Subnet
resource "aws_subnet" "private_subnet" {
  vpc_id = aws_vpc.vpc.id

  count             = length(var.private_subnet_cidr)
  cidr_block        = element(var.private_subnet_cidr, count.index)
  availability_zone = element(var.availability_zone, count.index)

  tags = {
    Name        = "${var.environment}-private_subnet-${element(var.availability_zone, count.index)}"
    Environment = "${var.environment}"
  }
}

# # Public Network ACL
# resource "aws_network_acl" "public_nacl" {
#   vpc_id = aws_vpc.vpc.id

#   subnet_ids = aws_subnet.public_subnet.*.id

#   tags = {
#     Name        = "${var.environment}_public_nacl"
#     Environment = "${var.environment}"
#   }
# }

# resource "aws_network_acl_rule" "public_nacl_inbound_rule" {
#   count = length(var.public_nacl_inbound_rule)

#   network_acl_id = aws_network_acl.public_nacl.id

#   rule_number = var.public_nacl_inbound_rule[count.index]["rule_number"]
#   rule_action = var.public_nacl_inbound_rule[count.index]["rule_action"]
#   from_port = lookup(var.public_nacl_inbound_rule[count.index], "from_port", null)
#   to_port = lookup(var.public_nacl_inbound_rule[count.index], "to_port", null)
#   protocol = lookup(var.public_nacl_inbound_rule[count.index], "protocol", null)
#   cidr_block = lookup(var.public_nacl_inbound_rule[count.index], "cidr_block", null)
# }

# # Private Network ACL
# resource "aws_network_acl" "private_nacl" {
#   vpc_id = aws_vpc.vpc.id

#   subnet_ids = aws_subnet.private_subnet.*.id

#   tags = {
#     Name        = "${var.environment}_private_nacl"
#     Environment = "${var.environment}"
#   }
# }

# resource "aws_network_acl_rule" "private_nacl_inbound_rule" {
#   count = length(var.private_nacl_outbound_rule)

#   network_acl_id = aws_network_acl.private_nacl.id

#   rule_number = var.private_nacl_outbound_rule[count.index]["rule_number"]
#   rule_action = var.private_nacl_outbound_rule[count.index]["rule_action"]
#   from_port = lookup(var.private_nacl_outbound_rule[count.index], "from_port", null)
#   to_port = lookup(var.private_nacl_outbound_rule[count.index], "to_port", null)
#   protocol = lookup(var.private_nacl_outbound_rule[count.index], "protocol", null)
#   cidr_block = lookup(var.private_nacl_outbound_rule[count.index], "cidr_block", null)
# }

# Internet Gateway
resource "aws_internet_gateway" "public_igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "${var.environment}_public_igw"
    Environment = "${var.environment}"
  }
}

# Public Route
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "${var.environment}-public-route"
    Environment = "${var.environment}"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id = aws_route_table.public.id
  gateway_id     = aws_internet_gateway.public_igw.id

  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "public_route" {
  count = length(aws_subnet.public_subnet)

  subnet_id      = element(aws_subnet.public_subnet.*.id, count.index)
  route_table_id = aws_route_table.public.id
}

# # NAT Gateway
# resource "aws_eip" "private_eip_nat" {
#   vpc = true
# }

# resource "aws_nat_gateway" "private_nat" {
#   subnet_id = element(aws_subnet.private_subnet.*.id, 0)
#   allocation_id = aws_eip.private_eip_nat.id

#   tags = {
#     Name        = "private_nat"
#     Environment = "${var.environment}"
#   }
# }

# resource "aws_route_table_association" "private_route" {
#   count = length(aws_subnet.private_subnet)

#   subnet_id      = element(aws_subnet.private_subnet.*.id, count.index)
#   route_table_id = aws_route_table.private.id
# }


# Private Route
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name        = "${var.environment}-private-route"
    Environment = "${var.environment}"
  }
}

# resource "aws_route" "private_nat_gateway" {
#   route_table_id = aws_route_table.private.id
#   nat_gateway_id = aws_nat_gateway.private_nat.id

#   destination_cidr_block = "0.0.0.0/0"
# }