module "tcrb-bof-logstash" {
  source = "../../../modules/secrets_manager"

  region      = var.region
  name        = var.name
  description = var.description
}