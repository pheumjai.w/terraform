output "vpc_id" {
  value = module.dev.vpc_id
}

output "internet_gateway_id" {
  value = module.dev.internet_gateway_id
}

# output "nat_gateway_id" {
#   value = module.dev.nat_gateway_id
# }

output "public_subnet_id" {
  value = module.dev.public_subnet_id
}

output "private_subnet_id" {
  value = module.dev.private_subnet_id
}