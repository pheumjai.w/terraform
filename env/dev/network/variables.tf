variable "profile" {
  description = "profile to deploy infrastructure"
}

variable "credential" {
  description = "path to aws credential"
}

variable "environment" {
  description = "infrastructure environment"
}

variable "region" {
  description = "infrastructure region"
}

variable "vpc_cidr" {
  description = "vpc cidr block"
}

variable "public_subnet_cidr" {
  description = "public subnet cidr block"
  type        = list(string)
}

variable "private_subnet_cidr" {
  description = "private subnet cidr block"
  type        = list(string)
}